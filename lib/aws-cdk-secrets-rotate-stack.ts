import cdk = require('@aws-cdk/core');
import { Duration } from '@aws-cdk/core';
import { Secret, } from '@aws-cdk/aws-secretsmanager';
import { Function, Runtime, Code } from '@aws-cdk/aws-lambda';
import { ServicePrincipal, PolicyStatement } from '@aws-cdk/aws-iam';
import { join } from 'path';

export class AwsCdkSecretsRotateStack extends cdk.Stack {

  private readonly secretName = "my-secret-name-here";
  private readonly keyInSecretName = 'keyInSecret';

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    /**
     * Managed Secret
     */
    const secret = new Secret(this, 'Secret', {
      description: "My Secret",
      secretName: this.secretName,
      generateSecretString: {
        secretStringTemplate: JSON.stringify({}),
        generateStringKey: this.keyInSecretName,
      }
    });

    /**
     * Lambda to rotate keys
     */
    const lambda = new Function(this, 'LambdaSecretRotate', {
      functionName: 'lambda-keys-rotate',
      runtime: Runtime.NODEJS_12_X,
      handler: 'index.handler',
      logRetention: 3,
      timeout: Duration.minutes(10),
      code: Code.fromAsset(join(__dirname, '..', 'lambda')),
      environment: {
        REGION: cdk.Stack.of(this).region,
        SECRET_NAME: this.secretName,
        KEY_IN_SECRET_NAME: this.keyInSecretName
      }
    })
    secret.grantRead(lambda);
    /**
     * Add schedule to rotate secrets
     */
    secret.addRotationSchedule('RotationSchedule', {
      rotationLambda: lambda,
      automaticallyAfter: Duration.hours(24)
    });
    /**
     * allow to be invoked by secrets manager
     */
    lambda.grantInvoke(new ServicePrincipal('secretsmanager.amazonaws.com'))
    /**
     * grantWrite secret by lambda
     */
    lambda.addToRolePolicy(new PolicyStatement({
      resources: [secret.secretArn],
      actions: ['secretsmanager:PutSecretValue']
    }));
  }
}
