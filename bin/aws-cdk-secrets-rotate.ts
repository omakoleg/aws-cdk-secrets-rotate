#!/usr/bin/env node
import 'source-map-support/register';
import cdk = require('@aws-cdk/core');
import { AwsCdkSecretsRotateStack } from '../lib/aws-cdk-secrets-rotate-stack';

const app = new cdk.App();
new AwsCdkSecretsRotateStack(app, 'AwsCdkSecretsRotateStack');
