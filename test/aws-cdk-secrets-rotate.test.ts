import { expect as expectCDK, haveResource, haveResourceLike } from '@aws-cdk/assert';
import { App } from '@aws-cdk/core';
import { AwsCdkSecretsRotateStack } from '../lib/aws-cdk-secrets-rotate-stack';

describe('AlertingStack', () => {
  const app = new App();
  const subject = new AwsCdkSecretsRotateStack(app, 'x');

  it('Contains ::Secret', () => {
    expectCDK(subject).to(haveResourceLike('AWS::SecretsManager::Secret', {
      Name: 'my-secret-name-here',
      GenerateSecretString: {
        SecretStringTemplate: JSON.stringify({}),
        GenerateStringKey: 'keyInSecret',
      }
    }));
  });

  it('Contains ::RotationSchedule', () => {
    expectCDK(subject).to(haveResource('AWS::SecretsManager::RotationSchedule', {
      RotationRules: {
        AutomaticallyAfterDays: 1
      }
    }));
  });

  it('Contains Lambda ::Role', () => {
    expectCDK(subject).to(haveResourceLike('AWS::IAM::Role', {
      AssumeRolePolicyDocument: {
        Statement: [
          {
            Action: "sts:AssumeRole",
            Effect: "Allow",
            Principal: {
              Service: "lambda.amazonaws.com"
            }
          }
        ]
      },
      ManagedPolicyArns: [
        {
          "Fn::Join": [
            "",
            [
              "arn:",
              {
                "Ref": "AWS::Partition"
              },
              ":iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
            ]
          ]
        }
      ]
    }));
  });

  it('Contains ::Policy for key access', () => {
    expectCDK(subject).to(haveResourceLike('AWS::IAM::Policy', {
      PolicyDocument: {
        Statement: [
          {
            Action: "secretsmanager:GetSecretValue",
            Effect: "Allow"
          },
          {
            Action: "secretsmanager:PutSecretValue",
            Effect: "Allow",
          }
        ]
      },
    }));
  });

  it('Contains ::Function', () => {
    expectCDK(subject).to(haveResourceLike('AWS::Lambda::Function', {
      Handler: "index.handler",
      Runtime: "nodejs12.x",
      Environment: {
        Variables: {
          REGION: {
            Ref: "AWS::Region"
          },
          SECRET_NAME: "my-secret-name-here",
          KEY_IN_SECRET_NAME: "keyInSecret"
        }
      },
      FunctionName: "lambda-keys-rotate",
      Timeout: 600
    }));
  });

  it('Contains LogRetention', () => {
    expectCDK(subject).to(haveResourceLike('Custom::LogRetention', {
      RetentionInDays: 3
    }));
  });

  it('Contains permission to call function by secretsmanager', () => {
    expectCDK(subject).to(haveResourceLike('AWS::Lambda::Permission', {
      Action: "lambda:InvokeFunction",
      Principal: "secretsmanager.amazonaws.com"
    }));
  });
});
